function recherche() {
  var tag = document.getElementById("input").value;
  var results = document.getElementById("results");
  results.textContent = "";

  var httpRequest = new XMLHttpRequest();

  httpRequest.onreadystatechange = function() {
    if (httpRequest.readyState == 4 && httpRequest.status == 200) {
      var res = JSON.parse(httpRequest.responseText);
      var ul = document.createElement("ul");
      results.appendChild(ul);

      for (var i = 0; i < res[3].length; i++) {
        var li = document.createElement("li");

        var titre = document.createElement("h1");
        var desc = document.createElement("a");
        var link = document.createElement("a");
        link.appendChild(document.createTextNode(" source"));
        link.title = "source";
        link.href = res[3][i];
        // desc = (document.createElement('p').textContent = res[2][i]);

        desc.appendChild(document.createTextNode(res[2][i]));
        titre.appendChild(document.createTextNode(res[1][i] + " : "));
        titre.title = res[1][i] + " :";
        titre.href = res[3][i];

        li.appendChild(titre);
        li.appendChild(desc);
        li.appendChild(link);
        var clone = li.cloneNode(true);
        ul.appendChild(clone);
      }
    }
    // document.querySelector('.results').innerHTML = httpRequest.responseText;
  };
  httpRequest.open(
    "GET",
    "http://localhost:1234/api?action=opensearch&format=json&formatversion=2&search=" +
      tag +
      "&namespace=0&limit=10&suggest=true"
  );
  httpRequest.send();
}
